import requests
import pandas as pd
import base64

data = []
page_nbr = 1

while True:
    # Decoded string parameter
    page_params = '{'+f'"language": "pt-br", "pageNumber": {page_nbr}, "pageSize": 40, "index": "IBOV", "segment": "1"'+'}'
    # Encoding to Base64
    encoded_page_params = base64.b64encode(page_params.encode()).decode()

    url = 'https://sistemaswebb3-listados.b3.com.br/indexProxy/indexCall/GetPortfolioDay/{}'
    response = requests.get(url.format(encoded_page_params))
    json_reponse = response.json()
    if json_reponse['results']:
        page_data = json_reponse['results']
        data.append(pd.DataFrame(page_data))
        print(len(page_data), 'rows has been extracted')
        page_nbr += 1
    else:
        print('all data has been extracted!')
        break

df = pd.concat(data, ignore_index=True)

columns = {'cod':'Código','asset':'Ação','type':'Tipo','part':'Part. (%)','theoricalQty':'Qtde. Teórica'}
df.rename(columns=columns,inplace=True)

df=df.dropna(axis=1,how='all')

print(df)
